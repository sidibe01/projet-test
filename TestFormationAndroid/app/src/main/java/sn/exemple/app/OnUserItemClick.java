package sn.exemple.app;

interface OnUserItemClick {

    void onClick(User user);

}