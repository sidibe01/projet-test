package sn.exemple.app;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

/**
 * Created on 7/12/18 at 3:31 PM
 * Project name : TestFormationAndroid
 */
@Database(entities = {User.class}, version = 1)
public abstract class UserDatabase extends RoomDatabase {

    public abstract UserDAO getUserDao();
}
