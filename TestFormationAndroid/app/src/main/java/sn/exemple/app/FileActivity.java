package sn.exemple.app;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

import java.io.File;
import java.io.FileOutputStream;

public class FileActivity extends AppCompatActivity {

    EditText edtFilename;

    EditText edtContent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_storage);

        edtFilename = findViewById(R.id.filename);
        edtContent = findViewById(R.id.content);
    }

    public void createFile(View view) {
        String fileName = edtFilename.getText().toString();
        if (TextUtils.isEmpty(fileName)) {
            return;
        }

        String content = edtContent.getText().toString();
        if (TextUtils.isEmpty(content)) {
            return;
        }

        FileOutputStream outputStream;

        try {
            File expresso = getDir("Expresso", Context.MODE_PRIVATE);
            File file = new File(expresso, fileName + ".txt");
            outputStream = new FileOutputStream(file);
            outputStream.write(content.getBytes());
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
