package sn.exemple.app;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;

public class MyService extends IntentService {

    public MyService() {
        super("MyService");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        Bundle extras = intent.getExtras();

        String action = extras.getString("action");

        if (action.equalsIgnoreCase("SEND_MAIL")) {
            String objet = extras.getString("objet");
            String message = extras.getString("message");
            sendMail(objet, message);
        }
    }

    private void sendMail(String objet, String message) {
        Log.d("Service", "sending mail. Objet: " + objet + ". \nMessage: " + message);
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }
}
