package sn.exemple.app;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 6/23/18 at 1:24 PM
 * Project name : TestFormationAndroid
 */
public class Tools {


    public static List<User> getUsers() {
        List<User> list = new ArrayList<>();
        list.add(new User(("Sidibe"), "Souleymane", "Developpeur"));
        list.add(new User(("Ba"), "Niaga", "Directeur Technique"));
        list.add(new User(("Gomis"), "Mario", "Lead Android"));
        list.add(new User(("Samb"), "Cheikhou", "Senior Android"));
        list.add(new User(("Diame"), "Serge", "Lead Android"));
        list.add(new User(("Mbengue"), "Babacar", "President Directeur General"));
        list.add(new User(("Diop"), "Astou", "Lead Android"));
        list.add(new User(("Sarr"), "Adja", "Directrice"));
        list.add(new User(("Sene"), "Paul", "Lead Android"));
        list.add(new User(("Sall"), "Macky", "President"));
        return list;
    }

}
