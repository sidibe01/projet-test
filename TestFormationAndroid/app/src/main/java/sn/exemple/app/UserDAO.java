package sn.exemple.app;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

/**
 * Created on 7/12/18 at 3:28 PM
 * Project name : TestFormationAndroid
 */
@Dao
public interface UserDAO {

    @Query("select * from user")
    List<User> getUsers();

    @Insert
    void insert(User user);

    @Insert
    void insertAll(User... user);
}
