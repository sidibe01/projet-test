package sn.exemple.app;

import java.util.List;

interface OnUsersLoaded {

    void onUsersLoaded(List<User> users);
}