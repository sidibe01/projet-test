package sn.exemple.app;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

public class RecyclerViewActivity extends AppCompatActivity implements UserAdapter.OnUserClick {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler_view);

        RecyclerView recyclerView = findViewById(R.id.id_activity_recycler_view);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        UserAdapter adapter = new UserAdapter(Tools.getUsers(), this);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onUserClick(User user) {
        Log.d("User", user.toString());
    }
}
