package sn.exemple.app;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

/**
 * Created on 6/23/18 at 1:21 PM
 * Project name : TestFormationAndroid
 */

public class UserAdapter2 extends RecyclerView.Adapter<UserAdapter2.AdapterViewHolder> {


    private List<User> users;

    private OnUserItemClick listener;

    public UserAdapter2(List<User> users, OnUserItemClick listener) {
        this.users = users;
        this.listener = listener;
    }

    @NonNull
    @Override
    public AdapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent,
            int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_layout, parent, false);
        return new AdapterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterViewHolder holder, int position) {
        User user = users.get(position);
        holder.prenom.setText(user.getPrenom());
        holder.nom.setText(user.getNom());
        holder.profession.setText(user.getProfession());
    }

    @Override
    public int getItemCount() {
        return users.size();
    }


    class AdapterViewHolder extends RecyclerView.ViewHolder {

        TextView nom;

        TextView prenom;

        TextView profession;

        public AdapterViewHolder(View itemView) {
            super(itemView);
            nom = itemView.findViewById(R.id.id_item_user_nom);
            prenom = itemView.findViewById(R.id.id_item_user_prenom);
            profession = itemView.findViewById(R.id.id_item_user_profession);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    User user = users.get(position);
                    listener.onClick(user);
                }
            });
        }
    }


}
