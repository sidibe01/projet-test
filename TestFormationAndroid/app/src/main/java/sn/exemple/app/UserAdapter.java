package sn.exemple.app;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

/**
 * Created on 6/22/18 at 4:54 PM
 * Project name : TestFormationAndroid
 */
public class UserAdapter extends RecyclerView.Adapter<UserAdapter.ViewHolder> {

    private List<User> users;

    private OnUserClick listener;

    public UserAdapter(List<User> users, OnUserClick listener) {
        this.users = users;
        this.listener = listener;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_layout, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        User user = users.get(position);
        holder.prenom.setText(user.getPrenom());
        holder.nom.setText(user.getNom());
        holder.profession.setText(user.getProfession());

    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView nom;

        TextView prenom;

        TextView profession;

        public ViewHolder(View itemView) {
            super(itemView);
            nom = itemView.findViewById(R.id.id_item_user_nom);
            prenom = itemView.findViewById(R.id.id_item_user_prenom);
            profession = itemView.findViewById(R.id.id_item_user_profession);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    User user = users.get(getAdapterPosition());
                    listener.onUserClick(user);
                }
            });
        }
    }

    interface OnUserClick {

        void onUserClick(User user);

    }
}
