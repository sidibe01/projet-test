package sn.exemple.app;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentB extends Fragment {

    EditText prenom;

    Button valider;

    public FragmentB() {// Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_blank_b, container, false);

        prenom = view.findViewById(R.id.edittext);
        valider = view.findViewById(R.id.valider);

        return view;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        valider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String strPrenom = prenom.getText().toString();
                if (strPrenom.isEmpty()) {
                    return;
                }

                Toast.makeText(getActivity(), strPrenom, Toast.LENGTH_LONG).show();
            }
        });
    }
}
