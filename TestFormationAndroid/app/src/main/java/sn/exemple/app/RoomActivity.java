package sn.exemple.app;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class RoomActivity extends AppCompatActivity implements OnUsersLoaded {

    EditText prenom;

    EditText nom;

    EditText profession;

    UserDAO dao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room);
        prenom = findViewById(R.id.id_room_prenom);
        nom = findViewById(R.id.id_room_nom);
        profession = findViewById(R.id.id_room_profession);

        dao = App.database.getUserDao();

        getUsers(this);
    }

    public void save(View view) {
        String strPrenom = prenom.getText().toString();
        String strNom = nom.getText().toString();
        String strProfession = profession.getText().toString();

        if (TextUtils.isEmpty(strPrenom) || TextUtils.isEmpty(strNom) || TextUtils
                .isEmpty(strProfession)) {

            return;
        }

        final User user = new User(strNom, strPrenom, strProfession);
        try {
            ExecutorService executorService = Executors.newSingleThreadExecutor();
            executorService.execute(new Runnable() {
                @Override
                public void run() {
                    dao.insert(user);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    void getUsers(final OnUsersLoaded onUsersLoaded) {
        try {
            ExecutorService executorService = Executors.newSingleThreadExecutor();
            executorService.execute(new Runnable() {
                @Override
                public void run() {
                    final List<User> users = dao.getUsers();
                    onUsersLoaded.onUsersLoaded(users);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onUsersLoaded(List<User> users) {
        for (User user : users) {
            Log.d("user", user.toString());
        }
    }

}
