package sn.exemple.app;

import android.app.Application;
import android.arch.persistence.room.Room;

/**
 * Created on 7/12/18 at 3:36 PM
 * Project name : TestFormationAndroid
 */
public class App extends Application {

    public static UserDatabase database;

    @Override
    public void onCreate() {
        super.onCreate();
        database = Room.databaseBuilder(getApplicationContext(), UserDatabase.class, "db")
                .build();
    }
}
