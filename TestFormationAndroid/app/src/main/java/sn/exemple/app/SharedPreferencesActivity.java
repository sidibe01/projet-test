package sn.exemple.app;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class SharedPreferencesActivity extends AppCompatActivity {

    public static final String EDACY_PREFS_NAME = "edacy_prefs";

    public static final String PREF_NAME_KEY = "name_key";

    EditText edtName;

    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shared_preferences);
        edtName = findViewById(R.id.id_shared_pref_nom);

        sharedPreferences = getSharedPreferences(EDACY_PREFS_NAME, Context.MODE_PRIVATE);
    }


    public void lire(View view) {
        String name = sharedPreferences.getString(PREF_NAME_KEY, "Doe");

        AlertDialog dialog = new AlertDialog.Builder(this)
                .setTitle("Preferences")
                .setMessage("Votre nom est " + name)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .create();
        dialog.show();
    }

    public void ecrire(View view) {
        String name = edtName.getText().toString();
        if (TextUtils.isEmpty(name)) {
            Toast.makeText(this, "Saisir votre nom", Toast.LENGTH_SHORT).show();
            return;
        }

        SharedPreferences.Editor edit = sharedPreferences.edit();
        edit.putString(PREF_NAME_KEY, name);
        edit.apply();
    }
}
