package sn.exemple.app;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

public class RecyclerViewActivity2 extends AppCompatActivity implements
        OnUserItemClick {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler);

        RecyclerView recyclerView = findViewById(R.id.id_recyclerView);
        recyclerView.setHasFixedSize(true);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);

        UserAdapter2 adapter = new UserAdapter2(Tools.getUsers(), this);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onClick(User user) {
        Toast.makeText(this, user.toString(), Toast.LENGTH_LONG).show();
    }
}
