package sn.exemple.app;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

/**
 * Created on 6/16/18 at 9:54 AM
 * Project name : TestFormationAndroid
 */
public class DemoActivity extends AppCompatActivity {

    TextView nom;

    TextView age;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demo);

        nom = findViewById(R.id.id_activity_demo_tv_nom);
        age = findViewById(R.id.id_activity_demo_tv_age);

        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {
            String name = bundle.getString("name");
            String age = bundle.getString("age");

            nom.setText(name);
            this.age.setText(age);
        }
    }
}
