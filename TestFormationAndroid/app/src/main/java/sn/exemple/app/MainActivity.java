package sn.exemple.app;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    EditText name;

    EditText age;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        name = findViewById(R.id.id_activity_test_edt_name);
        age = findViewById(R.id.id_activity_test_edt_age);
    }

    public void addFile(View view) {
        Intent intent = new Intent(this, FileActivity.class);
        startActivity(intent);
    }

    public void onClickValidate(View view) {
        if (view.getId() == R.id.id_activity_test_btn_validate) {
            Intent intent = new Intent(this, MainFragmentActivity.class);
            //recuperer les informations saisies
            String strName = name.getText().toString();
            String strAge = age.getText().toString();

            // Creer un objet bundle et y mettre les donnees a transmetre
            Bundle bundle = new Bundle();
            bundle.putString("name", strName);
            bundle.putString("age", strAge);
            intent.putExtras(bundle);

            startActivity(intent);
        } else if (view.getId() == R.id.id_activity_test_btn_service) {
            Intent intent = new Intent(this, RoomActivity.class);
//            intent.putExtra("action", "SEND_MAIL");
//            intent.putExtra("objet", "Formation");
//            intent.putExtra("message", "Bonjour,\n"
//                    + "ce samedi 23 Juin aura lieu notre 3e session dans les locaux de EXPRESSO : ");
            startActivity(intent);
        } else if (view.getId() == R.id.list) {
            Intent intent = new Intent(this, RecyclerViewActivity2.class);
            startActivity(intent);
        }
    }


    @Override
    protected void onStart() {
        super.onStart();
        Log.d("MainActivity", "onStart");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d("MainActivity", "onRestart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("MainActivity", "onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("MainActivity", "onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("MainActivity", "onStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("MainActivity", "onDestroy");
    }

    public void goToPrefs(View view) {
        Intent intent = new Intent(this, SharedPreferencesActivity.class);
        startActivity(intent);
    }
}
