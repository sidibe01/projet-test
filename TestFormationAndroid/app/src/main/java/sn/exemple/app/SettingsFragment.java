package sn.exemple.app;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.SwitchPreference;
import android.support.annotation.Nullable;

/**
 * Created on 7/14/18 at 12:23 PM
 * Project name : TestFormationAndroid
 */
public class SettingsFragment extends PreferenceFragment {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.pref_edacy);

        final SharedPreferences prefs = getActivity()
                .getSharedPreferences(SharedPreferencesActivity.EDACY_PREFS_NAME,
                        Context.MODE_PRIVATE);

        EditTextPreference name = (EditTextPreference) findPreference("key_name");
        SwitchPreference switchPref = (SwitchPreference) findPreference("key_switch");


        name.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                if (preference.getKey().equalsIgnoreCase("key_name")) {
                    String value = String.valueOf(newValue);

                    preference.setSummary("Votre nom est " + value);
                    SharedPreferences.Editor edit = prefs.edit();
                    edit.putString(SharedPreferencesActivity.PREF_NAME_KEY,
                            value);
                    edit.apply();
                }
                return true;
            }
        });
    }
}
